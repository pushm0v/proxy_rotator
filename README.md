# ProxyRotator

A simple tool to rotate proxy using [https://proxyrotator.com](proxyrotator.com) or load proxies list from a file.
This tool also make a simple request to test url (Configurable) to check whether a proxy is valid or not.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'proxy_rotator'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install proxy_rotator

## Usage

Include `proxy_rotator` gem in your `.rb` file
```ruby
require 'proxy_rotator'
```

## Configuration

```ruby
ProxyRotator.configure do |config|
  config.default_timeout = 5 # 5 seconds
  config.api_key = 'YOUR_PROXY_ROTATOR_API_KEY'
  config.default_test_url = 'https://google.com'
end
```

### Use proxyrotator.com service

```ruby
my_proxy = ProxyRotator.rotate_remote
```

Passing API parameters
```ruby
get:	true/false	Proxy supports GET requests
post	true/false	Proxy supports POST requests
cookies	true/false	Proxy supports cookies
referer	true/false	Proxy supports referer header
userAgent	true/false	Proxy supports user-agent header
port	integer	Return only proxies with specified port
city	string	Return only proxies with specified city
state	string	Return only proxies with specified state
country	string	Return only proxies with specified country
xml	true/false	Response will be in XML instead of jSON
```

Example
```ruby
config = {
    :get => true,
    :port => 8080,
    :city => 'New York',
    :state => 'NY',
    :country => 'US'
}

my_proxy = ProxyRotator.rotate_remote(config)
```

Get proxy information

```ruby
proxy_info = ProxyRotator.describe_remote(config)
```

### Load proxy list from a file

Example :

```ruby
http://username:password@1.2.3.4:5678
http://1.2.3.4:5678
```

```ruby
ProxyRotator.load_file('/your/proxy-list.txt')
my_proxy = ProxyRotator.rotate
```

#### Check proxy with test url

```ruby
my_proxy = ProxyRotator.rotate(true)
```

### Combine remote and rotate

```ruby
my_proxy = ProxyRotator.rotate_first_then_remote
```

```ruby
my_proxy = ProxyRotator.remote_first_then_rotate
```


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/pushm0v/proxy_rotator. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the ProxyRotator project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/[USERNAME]/proxy_rotator/blob/master/CODE_OF_CONDUCT.md).
