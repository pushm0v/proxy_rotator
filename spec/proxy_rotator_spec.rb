RSpec.describe ProxyRotator do
  it "has a version number" do
    expect(ProxyRotator::VERSION).not_to be nil
  end

  it "has a default base url configuration" do
    expect(ProxyRotator::Configuration::DEFAULT_BASE_URL).not_to be nil
  end

  it "has a api key configuration" do
    expect(ProxyRotator::Configuration::API_KEY).not_to be nil
  end
end
