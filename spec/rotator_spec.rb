module ProxyRotator
    RSpec.describe ProxyRotator do

      let(:proxies) { StringIO.new("http://1.2.3.4:56\nhttp://1.2.3.4:56\n") }

      it "raise error when can not load file" do
        expect {ProxyRotator.load_file("/non/exist/file")}.to raise_error(Errno::ENOENT)
      end

      it "can read file and return lines" do
        allow(File).to receive(:read).and_return(proxies)
      end

      it "validate url" do
        expect(ProxyRotator.validate_url("http://aa:bb@1.2.3.4:8080")).to eq(0)
      end
    end
end

