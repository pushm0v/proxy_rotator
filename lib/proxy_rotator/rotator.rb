require 'uri'
require 'rest-client'

module ProxyRotator
  @proxies = []
  @failed_proxies = []
  @start_index=0

  def self.load_file(filepath)
    @start_index=0
    File.open(filepath, "r") do |f|
      f.each_line do |line|
        unless validate_url(line).nil? || @proxies.include?(line)
          @proxies << line.strip
        end
      end
    end
  end

  def self.reset_rotate(check_first=false)
    @start_index = 0
    self.rotate(check_first)
  end

  def self.rotate(check_first=false)
    return nil unless @proxies.count > 0
    proxy = @proxies[@start_index]
    @start_index=@start_index+1

    if check_first
      begin
        RestClient::Request.new(
            :method => :get,
            :url => ProxyRotator.configuration.default_test_url,
            :proxy => proxy,
            :timeout => ProxyRotator.configuration.default_timeout
        ).execute

        return proxy
      rescue Exception => e
        p "Proxy #{proxy} failed : #{e.message}"
        @failed_proxies << proxy unless @failed_proxies.include? proxy
        return nil unless @proxies.count > 1 || @proxies.count == @failed_proxies.count
        self.rotate(true)
      end
    else
      return proxy
    end
  end

  def self.base_url(config={})
    url = ProxyRotator.configuration.base_url + "?apiKey=" + ProxyRotator.configuration.api_key
    url = url + "&" + config.to_a.map { |x| "#{x[0]}=#{x[1]}" }.join("&") unless config.empty?

    return url
  end

  def self.rotate_remote(config={})
    url = self.base_url(config)

    json = self.get_proxyrotator_proxy(url)
    proxy = json["proxy"]
    return "http://#{proxy}" unless proxy.nil?

    return nil
  end

  def self.describe_remote(config={})
    url = self.base_url(config)
    self.get_proxyrotator_proxy(url)
  end

  def self.get_proxyrotator_proxy(url)
    response = RestClient.get(url)
    JSON.parse(response.body)
  end

  def self.rotate_first_then_remote(check_first=false, config={})
    result = self.rotate(check_first)
    result = self.rotate_remote(config) unless !result.nil?

    result
  end

  def self.remote_first_then_rotate(check_first=false, config={})
    result = self.rotate_remote(config)
    result = self.rotate(check_first) unless !result.nil?

    result
  end

  def self.validate_url(url)
    url =~ URI::regexp(%w(http https))
  end
end