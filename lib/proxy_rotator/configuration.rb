require 'ostruct'

module ProxyRotator
  class Configuration

    DEFAULT_BASE_URL = 'http://api.proxyrotator.com/'
    API_KEY = ''
    DEFAULT_TEST_URL = 'https://www.google.com'
    DEFAULT_TIMEOUT = 5 #in seconds

    def initialize
      @configuration = ::OpenStruct.new
    end

    def base_url
      @configuration.base_url || DEFAULT_BASE_URL
    end

    def base_url=(host)
      @configuration.base_url = host
    end

    def api_key
      @configuration.api_key || API_KEY
    end

    def api_key=(api_key)
      @configuration.api_key = api_key
    end

    def default_timeout
      @configuration.default_timeout || DEFAULT_TIMEOUT
    end

    def default_timeout=(timeout)
      @configuration.default_timeout = timeout
    end

    def default_test_url
      @configuration.default_test_url || DEFAULT_TEST_URL
    end

    def default_test_url=(url)
      @configuration.default_test_url = url
    end
  end

  def self.configuration
    @configuration ||= initialize_configuration!
  end

  def self.configure
    yield(configuration)
  end

  def self.initialize_configuration!
    @configuration = Configuration.new
  end
end